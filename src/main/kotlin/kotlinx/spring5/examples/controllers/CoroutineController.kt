package kotlinx.spring5.examples.controllers

import kotlinx.coroutines.experimental.Unconfined
import kotlinx.coroutines.experimental.reactive.awaitSingle
import kotlinx.coroutines.experimental.reactor.mono
import kotlinx.spring5.examples.persistence.ReactiveAuditRepository
import kotlinx.spring5.examples.persistence.ReactiveMessageRepository
import kotlinx.spring5.examples.persistence.ReactivePeopleRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

/**
 * CoroutineController
 * @author debop@coupang.com
 * @since 17. 7. 25
 */
@RestController
class CoroutineController(val peopleRepo: ReactivePeopleRepository,
                          val auditRepo: ReactiveAuditRepository,
                          val messageRepo: ReactiveMessageRepository) {

  @GetMapping("/coroutine/{personId}")
  fun getMessages(@PathVariable personId: String): Mono<String> = mono(Unconfined) {
    val person = peopleRepo.findById(personId).awaitSingle()
    val lastLogin = auditRepo.findOneByEmail(person.email).awaitSingle().eventDate
    val numberOfMessages = messageRepo.countByMessageDateGreaterThanAndEmail(lastLogin, person.email).awaitSingle()

    "Hello ${person.name}, you have $numberOfMessages message since $lastLogin"
  }
}