package kotlinx.spring5.examples.controllers

import kotlinx.spring5.examples.persistence.ReactiveAuditRepository
import kotlinx.spring5.examples.persistence.ReactiveMessageRepository
import kotlinx.spring5.examples.persistence.ReactivePeopleRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

/**
 * ReacitveController
 * @author debop@coupang.com
 * @since 17. 7. 25
 */
@RestController
class ReacitveController(val peopleRepo: ReactivePeopleRepository,
                         val auditRepo: ReactiveAuditRepository,
                         val messageRepo: ReactiveMessageRepository) {

  @GetMapping("/reactive/{personId}")
  fun getMessages(@PathVariable personId: String): Mono<String> {
    return peopleRepo.findById(personId)
        .switchIfEmpty(Mono.error(NoSuchElementException()))
        .flatMap { person ->
          auditRepo.findOneByEmail(person.email)
              .flatMap { lastLogin ->
                messageRepo.countByMessageDateGreaterThanAndEmail(lastLogin.eventDate, person.email)
                    .map { numberOfMessages ->
                      "Hello ${person.name}, you have $numberOfMessages message since ${lastLogin.eventDate}"
                    }
              }
        }
  }
}