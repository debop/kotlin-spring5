package kotlinx.spring5.examples.controllers

import kotlinx.spring5.examples.persistence.AuditRepository
import kotlinx.spring5.examples.persistence.MessageRepository
import kotlinx.spring5.examples.persistence.PeopleRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

/**
 * BlockingController
 * @author debop@coupang.com
 * @since 17. 7. 25
 */
@RestController
class BlockingController(val peopleRepo: PeopleRepository,
                         val auditRepo: AuditRepository,
                         val messageRepo: MessageRepository) {

  @GetMapping("/blocking/{personId}")
  fun getMessageFor(@PathVariable personId: String): String {
    val person = peopleRepo
        .findById(personId)
        .orElseThrow { NoSuchElementException("Not found personId=$personId") }

    val lastLogin = auditRepo.findOneByEmail(person.email).eventDate
    val numberOfMessages = messageRepo.countByMessageDateGreaterThanAndEmail(lastLogin, person.email)

    return "Hello ${person.name}, you have $numberOfMessages message since $lastLogin"
  }
}