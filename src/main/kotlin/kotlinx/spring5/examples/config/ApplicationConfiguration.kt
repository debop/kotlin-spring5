package kotlinx.spring5.examples.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

/**
 * ApplicationConfiguration
 * @author debop@coupang.com
 * @since 17. 7. 25
 */
@Configuration
class ApplicationConfiguration {

  @Bean fun clock(): Clock = Clock.systemDefaultZone()
}