package kotlinx.spring5.examples.handlers

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

/**
 * GlobalControllerExceptionHandler
 * @author debop@coupang.com
 * @since 17. 7. 25
 */
@ControllerAdvice
class GlobalControllerExceptionHandler {

  @ExceptionHandler(NoSuchElementException::class)
  fun return404(): ResponseEntity<String> = ResponseEntity.notFound().build<String>()
}