package kotlinx.spring5.examples.persistence

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

@Repository
interface PeopleRepository : CrudRepository<Person, String>

@Repository
interface AuditRepository : CrudRepository<Audit, String> {
  fun findOneByEmail(email: String): Audit
}

@Repository
interface MessageRepository : CrudRepository<Message, String> {
  fun countByMessageDateGreaterThanAndEmail(messageDate: LocalDateTime, email: String): Long
}